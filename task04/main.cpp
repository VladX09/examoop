#include <iostream>
#include <string>
using namespace std;

class Element{
 protected:
     //Fields
     bool* pointer1;    //Connected elements
     bool* pointer2;
     bool pin1SetFlag;    //If input values are setted
     bool pin2SetFlag;
     bool pointer1SetFlag;  //If elements are connected
     bool pointer2SetFlag;

 public:
     //Fields
     bool pin1;
     bool pin2;
     bool pin3;

     //Constructors
     Element(){
         pin1 = false;
         pin2 = false;
         pin1SetFlag = false;
         pin2SetFlag = false;
     }
     Element(bool p1, bool p2){
         pin1 = p1;
         pin2 = p2;
         pin1SetFlag = true;
         pin2SetFlag = true;
     }

    //Methods
     void Set(int num, bool val){
         switch(num){
         case 1:
             pin1 = val;
             pin1SetFlag = true;
             break;
         case 2:
             pin2 = val;
             pin2SetFlag = true;
             break;
         default:
             throw(string("undefinite pin"));
         }
     }
     void Connect(Element &out, int input){
         switch(input){
         case 1:
             out.Calculate();
             pointer1 = &out.pin3;
             pointer1SetFlag = true;
             break;
         case 2:
             out.Calculate();
             pointer2 = &out.pin3;
             pointer2SetFlag = true;
             break;
         default:
             throw(string("undefinite input value"));
             break;
         }
     }
     void Prepare(){
         if(!pin1SetFlag){
             if(!pointer1SetFlag){
                 throw(string("Set or connect elements"));
             }
             pin1 = *pointer1;
             pin1SetFlag = true;
         }
         if(!pin2SetFlag){
             if(!pointer2SetFlag){
                 throw(string("Set or connect elements"));
             }
             pin2 = *pointer2;
             pin2SetFlag = true;
         }
     }

     virtual bool Calculate()=0;

 };


class AND: public Element{
public:
    AND():Element(){}
    AND(bool p1, bool p2):Element(p1,p2){}
    bool Calculate(){
        Prepare();
        pin3 = pin1 && pin2;
        return pin3;
    }
};

class OR: public Element{
public:
    OR():Element(){}
    OR(bool p1, bool p2):Element(p1,p2){}
    bool Calculate(){
        Prepare();
        pin3 = pin1 || pin2;
        return pin3;
    }
};

class NOT: public Element{
public:
    NOT():Element(){}
    NOT(bool p2):Element(false,p2){}

    void Prepare(){
        if(!pin2SetFlag){
            if(!pointer2SetFlag){
                throw(string("Set or connect elements"));
            }
            pin2 = *pointer2;
            pin2SetFlag = true;
        }
    }
    bool Calculate(){
        Prepare();
        pin3 = !pin2;
        return pin3;
    }
};

int main()
{
    try{
        AND a(false,true);
        OR o;
        o.Connect(a,1);
        o.Set(2, false);

        NOT n;
        n.Connect(o,2);


        cout << n.Calculate() << endl;
    }
    catch(string s){
        cout << s << endl;
        return 0;
    }

    return 0;
}

