/**
* Даны несколько геометрических фигур: линия, треугольник и квадрат.
* Выделите общие свойства и методы фигур в виртуальный базовый класс
* и предоставьте интерфейс для вращения фигур относительно заданной точки
* отобразите фигуры в текстовом виде
*
 */

#include <iostream>
#include <cmath>
#include <vector>

using namespace std;

struct sPoint {
    double x;
    double y;
    sPoint(double X, double Y) {
        x = X;
        y = Y;
    }
};

double getDistance(sPoint& a, sPoint& b) {
    return sqrt(pow(a.x - b.x, 2.0) + pow(a.y - b.y, 2.0));
}

// Общее свойство фигур -- у них есть N точек
class BaseFigure {
protected:
    vector<sPoint> mPoints;

public:

    virtual double calculateArea() const = 0;
    virtual double calculatePerimeter() const = 0;

    sPoint getPoint(int idx) const {
        return mPoints[idx];
    }

    // Поворот одинаков для всех фигур -- поочередный поворот всех точек
    virtual void rotate(sPoint base, double angle) {

        for (vector<sPoint>::iterator it = mPoints.begin(); it != mPoints.end(); it++) {
            sPoint& curr = *it;
            *it = sPoint(
                    base.x + (curr.x - base.x) * cos(angle) - (curr.y - base.y) * sin(angle),
                    base.y + (curr.y - base.y) * cos(angle) - (curr.x - base.x) * sin(angle)

            );
        }

    }

    virtual void draw() = 0;

};


// Строим прямую по двум точкам
class Line : public BaseFigure {
public:
    Line(const sPoint& start, const sPoint& end)  {
        mPoints.push_back(start);
        mPoints.push_back(end);

    }

    double calculateArea() const {
        return 0;
    }

    double calculatePerimeter() const {
        return getDistance(mPoints[0], mPoints[1]);
    }

    virtual void draw() {
        cout << "Line" << endl;
    }
};


// Треугольник строим по трем точкам
class Triangle: public BaseFigure {
public:
    Triangle(const sPoint& a, const sPoint& b, const sPoint& c) {
        mPoints.push_back(a);
        mPoints.push_back(b);
        mPoints.push_back(c);
    }

    double calculateArea() const {
        double p = calculatePerimeter() / 2.0;
        double a = getDistance(mPoints[0], mPoints[1]);
        double b = getDistance(mPoints[1], mPoints[2]);
        double c = getDistance(mPoints[0], mPoints[2]);
        return sqrt(p * (p - a) * (p - b) * (p - c));
    }

    double calculatePerimeter() const {
        return getDistance(mPoints[0], mPoints[1]) + getDistance(mPoints[1], mPoints[2]) + getDistance(mPoints[0], mPoints[2]);
    }

    virtual void draw() {
        cout << "Triangle" << endl;
    }
};

// Строим квадрат по одной точке (нижней левой) и углу поворота
class Square : public BaseFigure {
public:
    Square(const sPoint& base, double side, double angle) {
        double dx = side * cos(angle);
        double dy = side * sin(angle);

        mPoints.push_back(base);
        mPoints.push_back(sPoint(base.x, base.y + dy));
        mPoints.push_back(sPoint(base.x + dx, base.y));
        mPoints.push_back(sPoint(base.x + dx, base.y + dy));
    }

    double calculateArea() const {
        return getSide() * getSide();
    }

    double calculatePerimeter() const {
        return getSide() * 4;
    }

    double getSide() const {
        return getDistance(mPoints[0], mPoints[1]);
    }

    virtual void draw() {
        cout << "Square" << endl;
    }

};


int main() {
    Line a(sPoint(0.0, 0.0), sPoint(1, 1));
    Triangle b(sPoint(0, 0), sPoint(1, 1), sPoint(0, 1));
    Square c(sPoint(0,0), 2, 0);
    return 0;
}