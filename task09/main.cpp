/**
* Сделать пул, перегрузить << и >>
*/

#include <iostream>
#include <list>

using namespace std;


class Inventory {
private:
    string mName;
    string mSerial;
public:
    Inventory() {

    }

    Inventory(string name, string serial) {
        mName = name;
        mSerial = serial;
    }

    string getName() const {
        return mName;
    }

    string getSerial() const {
        return mSerial;
    }

    friend InvPool& operator>>(InvPool&, Inventory&);
};

class InvUser {
private:
    string mName;
public:
    InvUser(string name) {
        mName = name;
    }
};

class InvPool {
private:
    list<Inventory> mPool;
    list<InvUser> mUsers;
public:
    InvPool& operator<<(const Inventory& item) {
        mPool.push_back(item);
        return *this;
    }
    friend InvPool& operator>>(InvPool&, Inventory&);

    InvPool& operator<<(InvUser& src) {
        mUsers.push_back(src);
    }

    void clear() {
        mPool.clear();
        mUsers.clear();
    }
};

InvPool& operator>>(InvPool& pool, Inventory& target) {
    Inventory& a = pool.mPool.back();
    pool.mPool.pop_back();
    target = a;
}

int main() {
    InvPool skiInv;
    Inventory ski("Ski", "0111");
    Inventory ski2("Ski", "0112");
    Inventory ski3("Ski", "0113");
    Inventory ski4("Ski", "0114");
    Inventory ski5("Ski", "0115");
    Inventory ski6("Ski", "0116");
    Inventory ski7("Ski", "0117");

    InvUser V("Vasily"), V2("Vasily V"), V3("Ivan");

    skiInv << V << V2 << V3;

    skiInv << ski << ski2 << ski3 << ski4 << ski5 << ski6 << ski7;

    Inventory getSki;
    skiInv >> getSki;

    skiInv.clear();

    return 0;
}