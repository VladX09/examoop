/**
* с помощью шаблона multimap создайте редактируемый англо-русский словарь.
* обеспечьте его работу в двухстороннем порядке: русско-английском и англо-русском с предсказанием ввода:
* поссле ввода очередного символа с клавиатуры выводится подсказка из начинающихся с введенной подстроки слов
* (до 10-ти)
*/

#include <iostream>
#include <string>
#include <set>
#include <map>
using namespace std;

class Dictionary {
private:
    // key -- the word
    // value -- translation
    multimap<string, string> eng;
    multimap<string, string> rus;

public:
    void addWord(string english, string russian) {
        // добавляем слово в английский и в русский словари
        eng.insert(pair<string,string>(english, russian));
        rus.insert(pair<string,string>(russian, english));
    }

    /**
    * Переводит русское слово на английский
    * возвращает набор со значениями
    */
    set<string> translate(string word) const {
        set<string> result;
        for (auto it : rus) {
            if (it.first == word) {
                result.insert(it.second);
            }
        }
        for (auto it : eng) {
            if (it.first == word) {
                result.insert(it.second);
            }
        }
        return result;
    }

    /**
    * Предугадывание для слов в русском словаре (рус->англ)
    */
    set<string> search(string substr, unsigned short int max = 10) const {
        set<string> result;
        unsigned short int i = 0;
        for (auto it : rus) { // c++11
            if (i++ >= max) {
                break;
            }
            if (it.first.find(substr) == 0) { // если подстрока в начале
                result.insert(it.first); // добавляем ключ
            }
        }
        for (auto it : eng) {
            if (i++ >= max) {
                break;
            }
            if (it.first.find(substr) == 0) { // если подстрока в начале
                result.insert(it.first); // добавляем ключ
            }
        }
        return result;
    }

};

int main() {
    Dictionary dic;
    string word, translation;
    cout << "Hello, let's fill up the dictionary. Type STOP when we're done." << endl;
    while (true) {
        cout << "English: ";
        cin >> word;
        if (word == "STOP") {
            break;
        }
        cout << "Russian: ";
        cin >> translation;
        dic.addWord(word, translation); // добавляем слова
    }

    // показывает принцип работы подсказки
    cout << "Search for word: ";
    cin >> word;
    for (auto it : dic.search(word)) { // ищем слова, начинающиеся с подстроки
        cout << it << ":" << endl;
        for (auto word : dic.translate(it)) { // для каждого слова ищем возможные переводы и выводим их
            cout << word << endl;
        }
    }

    return 0;
}