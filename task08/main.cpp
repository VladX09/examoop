#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <utility>
#include <algorithm>
using namespace std;

//functions for filling
map<char,int> initOut() {
    map<char,int>outputs;
    char name = 'a';
    int number = 0;
    while (name <= 'z'){
        outputs.insert(pair<char,int>(name,number));
        number++;
        name++;
    }
    return outputs;
}

map<char,int> initOut(char names[10], int values[10]) {
    map<char,int>outputs;
    for(int i = 0; i < 10; i++) {
        outputs.insert(pair<char,int>(names[i],values[i]));
    }
    return outputs;
}

map<char,string> initCat() {
    map<char,string>categories;
    char name = 'z';        //ATTENTION: map is filled in the back order
    int i = 0;
    string cteg_names[8] = {"power", "analog in", "analog out", "analog in/out", "digital in",
                          "digital out", "digital in/out", "not connected"};
    while (name >= 'a'){
        int n = i%8;
        categories.insert(pair<char,string>(name,cteg_names[n]));
        i++;
        name--;
    }
    return categories;
}

//functions for printing
void printMap(map<char,int> &m) {
    for(map<char,int>::iterator it = m.begin(); it != m.end(); it++) {
        cout << it->first << ":" << it->second << endl;
    }
    cout << "___________ \n" << endl;
}

void printMap(map<char,string> &m) {
    for(map<char,string>::iterator it = m.begin(); it != m.end(); it++) {
        cout << it->first << ":" << it->second << endl;
    }
    cout << "___________ \n" << endl;
}

//Prints category
void printCat(map<char,string> &source, string category, map<char,int> &outs) {
    int out = -1;
    for(map<char,string>::iterator it = source.begin(); it != source.end(); it++) {
        if(!it->second.compare(category)) {
            for (map<char,int>::iterator iter = outs.begin(); iter != outs.end(); iter++) {
                if(it->first == iter->first) {
                    out = iter->second;
                }
            }
            cout << it->first << ":" << it->second << ":" << out << endl;
        }
    }
    cout << "___________ \n" << endl;
}

//Sorts by output value
bool cmpr(const pair<char,int> &a, const pair<char,int> &b) {
    return a.second < b.second;
}

void sortOut(map<char,int> &outs) {
    vector<pair<char,int> > tmp(outs.begin(),outs.end());
    sort(tmp.begin(),tmp.end(),cmpr);
    for(vector<pair<char,int> >::iterator it = tmp.begin(); it != tmp.end(); it++) {
        cout << it->first << ":" << it->second << endl;
    }
    cout << "___________ \n" << endl;
}

int main() {
    char names[10];
    names[0] = 'a';
    for(int i = 1; i<10; i++) {
        names[i] = names[i-1]+1;
    }
    int values [10] = {10, 5, 12, 45, 11, 8, 0, 5, 9, 4};
    map<char,int>outs1 = initOut(names,values);

    printMap(outs1);
    sortOut(outs1);

    map<char,int>outs2 = initOut();
    map<char,string>categories = initCat();

    printCat(categories, "power", outs2);

    return 0;
}



