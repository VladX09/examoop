/* Задача «Матрица» (перегрузка операций и функций)
Запрограммируйте класс M, с объектами которого можно выполнить следующие операции:
M m;// создание матрицы 3 х 3 с нулевыми элементами
M n(2,2);// создание матрицы 2х2 с нулевыми элементами
M o(1); // создание матрицы 2х2 с единичной диагональю
M p(“1 2 3 4 5 6 7 8 9”);// создание квадратной матрицы ,
// значения элементов 0...9 разделяются пробелами
M q = m + p; // сложение двух матриц
//и помещение результата в третью
n – o; // вычитание матриц
5 + n; // добавление константы ко всем элементам матрицы
n – 5;// вычитание константы из всех элементов матрицы
*/

#include <iostream>
#include <string>
#include <sstream>
#include <iterator>
#include <vector>
using namespace std;

class SquareMatrix {

private:
    int mRows;
    int mCols;
    int ** mData;

public:
    SquareMatrix() {
        mRows = 3;
        mCols = 3;
        mData = new int*[mRows];
        for (int i = 0; i < mRows; i++) {
            mData[i] = new int[mCols]();
        }
    }

    SquareMatrix(int rows, int cols) {
        mRows = rows;
        mCols = cols;
        mData = new int*[mRows];
        for (int i = 0; i < mRows; i++) {
            mData[i] = new int[mCols]();
        }
    }

    SquareMatrix(int diag) {
        mRows = 2;
        mCols = 2;
        mData = new int*[mRows];
        for (int i = 0; i < mRows; i++) {
            mData[i] = new int[mCols]();
            mData[i][i] = diag;
        }
    }

    SquareMatrix(string data) {
        istringstream in(data);

        mRows = 3;
        mCols = 3;

        mData = new int*[mRows];
        for (int i = 0; i < mRows; i++) {
            mData[i] = new int[mCols]();
            for (int j = 0; j < mCols; j++) {
                in >> mData[i][j];
            }
        }
    }

    int getRows() const {
        return mRows;
    }

    int getCols() const {
        return mCols;
    }


    void set(int i, int j, int data) {
        if (!(isPositionValid(i, j))) {
            return;
        }
        mData[i][j] = data;
    }

    int get(int i, int j) const {
        if (!(isPositionValid(i, j))) {
            return 0;
        }
        return mData[i][j];
    }

    void print() const {
        for (int i = 0; i < getRows(); i++) {
            for (int j = 0; j < getCols(); j++) {
                cout << get(i,j) << "\t";
            }
            cout << endl;
        }
    }


    SquareMatrix& operator-(int src) {
        for (int i = 0; i < getRows(); i++) {
            for (int j = 0; j < getCols(); j++) {
                set(i, j, get(i,j) + src);
            }
        }
        return *this;
    }

    SquareMatrix& operator-(const SquareMatrix& src) {
        for (int i = 0; i < getRows(); i++) {
            for (int j = 0; j < getCols(); j++) {
                set(i, j, get(i,j) - src.get(i, j));
            }
        }
        return *this;
    }




protected:
    bool isPositionValid(int i, int j) const {
        return (i >= 0 && i < getRows() && j >= 0 && j < getCols());
    }

};


// Оператор не friend потому что не получаем доступ к private-данным
SquareMatrix& operator+(int src, SquareMatrix& dest) {
    for (int i = 0; i < dest.getRows(); i++) {
        for (int j = 0; j < dest.getCols(); j++) {
            dest.set(i, j, dest.get(i,j) + src);
        }
    }
    return dest;
}


SquareMatrix& operator+(const SquareMatrix& A, const SquareMatrix& B) {
    SquareMatrix result(A.getRows(), A.getCols());
    for (int i = 0; i < A.getRows(); i++) {
        for (int j = 0; j < A.getCols(); j++) {
            result.set(i, j, A.get(i,j) + B.get(i, j));
        }
    }
    return result;
}


int main() {
    SquareMatrix m;// создание матрицы 3 х 3 с нулевыми элементами
    SquareMatrix n(2,2);// создание матрицы 2х2 с нулевыми элементами
    SquareMatrix o(1); // создание матрицы 2х2 с единичной диагональю
    SquareMatrix p("1 2 3 4 5 6 7 8 9");// создание квадратной матрицы ,третью
    // значения элементов 0...9 разделяются пробелами
    SquareMatrix q = m + p; // сложение двух матриц
    //и помещение результата в третью
    n - o; // вычитание матриц
    5 + n; // добавление константы ко всем элементам матрицы
    n - 5;// вычитание константы из всех элементов матрицы
    return 0;
}