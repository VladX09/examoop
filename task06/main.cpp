#include <iostream>
#include <string>
#include <ctime>
#include <fstream>
#include <vector>
#include <cstring>
#include <algorithm>
using namespace std;



class Record {
private:
    int id;
    char creationDate[100];
    char name[50];
    int value;

    static void pushVect(vector<Record> &tmp) {
        ofstream out("data.txt",ios::binary|ios::trunc|ios::out|ios::ate);
        for(vector<Record>::iterator it = tmp.begin(); it < tmp.end(); it++) {
            out.write((char*)&(*it),sizeof *it);
        }
        out.close();
    }

    static bool sortUp(Record a, Record b) {
        return (a.getValue() < b.getValue());
    }
    static bool sortDwn(Record a, Record b) {
        return (a.getValue() > b.getValue());
    }

public:
    //Fields
    static int commonId;

    //Constructor
    Record(): Record(0,""){}
    Record(int val, string nam) {
        id = commonId++;
        value = val;
        strcpy(name, nam.c_str());
        struct tm *strTime;
        time_t It;
        It = time(NULL);
        strTime = localtime(&It);
        strcpy(creationDate, asctime(strTime));
    }

    //Setter's n getter's
    int getId() {
        return id;
    }

    string getName() {
        return string(name);
    }

    string getCreationDate() {
        return string(creationDate);
    }

    int getValue() {
        return value;
    }

    //Methods
        //Static
    static void truncFile() {
        ofstream o("data.txt", ios::binary|ios::trunc);
        o.close();
    }

    static void printFile() {
        ifstream in("data.txt", ios::binary|ios::in);
        Record tmpRec;
        while(in.read((char*)&tmpRec, sizeof tmpRec)){
            cout << tmpRec.name << endl << tmpRec.value << endl <<
                    tmpRec.creationDate << endl << endl;
        }
        in.close();
        cout << "------" << endl;
    }

     static int search(string name){
        ifstream in("data.txt",ios::binary|ios::in);
        Record tmpRec;
        while(in.read((char*)&tmpRec, sizeof tmpRec)) {
            if(!name.compare(tmpRec.getName())) {
                return tmpRec.getId();
                in.close();
            }
        }
        return -1;
        in.close();
    }

     static void replace(int recId, Record& rec) {
         if (recId >= 0) {
             vector<Record>tmp;
             ifstream in("data.txt",ios::binary|ios::in);
             Record tmpRec;
             while(in.read((char*)&tmpRec, sizeof tmpRec)) {
                 if(tmpRec.getId() != recId) {
                     tmp.push_back(tmpRec);
                 } else {
                     tmp.push_back(rec);
                 }
             }
             in.close();
             pushVect(tmp);
         }
     }

     static void replace(string name, Record& rec) {
         int recId = search(name);
         replace(recId, rec);
     }

     static void sortByVal(bool up) {
         vector<Record>tmp;
         ifstream in("data.txt",ios::binary|ios::in);
         Record tmpRec;
         while(in.read((char*)&tmpRec, sizeof tmpRec)) {
                 tmp.push_back(tmpRec);
         }
         in.close();
         if(up){
            sort(tmp.begin(),tmp.end(),sortUp);
         } else{
            sort(tmp.begin(),tmp.end(),sortDwn);
         }
         pushVect(tmp);

     }

        //Non-static
     void add() {
         ofstream out("data.txt",ios::binary|ios::out|ios::app);
         out.write((char*)this, sizeof *this);
         out.close();
     }

     void del() {
         vector<Record>tmp;
         ifstream in("data.txt",ios::binary|ios::in);
         Record tmpRec;
         while(in.read((char*)&tmpRec, sizeof tmpRec)) {
             if(tmpRec.getId() != id) {
                 tmp.push_back(tmpRec);
             }
         }
         in.close();
         pushVect(tmp);
     }
};

int Record::commonId = 0;

int main() {
    //truncate file
    Record::truncFile();

    //create
    Record rec1(3,string("First"));
    Record rec2(2,string("Second"));
    Record rec3(1,string("Third"));
    Record rec4(4,string("4th"));

    //add
    rec1.add();
    rec2.add();
    rec3.add();
    rec4.add();
    Record::printFile();

    //delete
    rec3.del();
    Record::printFile();

    //find'n'replace
    Record::replace(0, rec2);
    Record::printFile();

    //sort by value
    Record::sortByVal(true);
    Record::printFile();

    Record::sortByVal(false);
    Record::printFile();
    return 0;
}

