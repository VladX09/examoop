#include <iostream>
#include <list>
#include <iterator>
#include <ctime>
#include <random>
#include <string>
#include <cstring>
#include <fstream>
#include <vector>
using namespace std;

//Function to get list of files
list<string> myGetFileNames(string name) {
    list<string> files;
    for(int i = 0; i < 5; i++) {
        files.push_back(name.append(to_string(i)));
        name.pop_back();
    }
    return files;
}

//Truncate files
void truncFile(bool tag) {
    string name = (tag)?"tags.txt":"connections.txt";
    ofstream file(name,ios_base::trunc);
    file.close();
}

//Class Tag
class Tag {
private:
    //fields
    char name[15];
public:
    //constructors
    Tag(string str = "") {
        int c = 15;
        for(int i = 0; i<c; i++) {
            name[i] = ' ';
        }
        strcpy(name,str.c_str());
    }
    //setters and getters
    void setName(string str) {
        int c = 15;
        for(int i = 0; i<c; i++) {
            name[i] = ' ';
        }
        strcpy(name,str.c_str());
    }

    string getName() const {
        return string(name);
    }

    //methods
    void write() {
        ofstream out("tags.txt", ios::app);
        out << name << endl;
        out.close();
    }

};

//Class File
class File {
private:
    //fields
    char name[10];
public:
     Tag tags[10];
     int count;
     bool hasTag;
    //fields
    //constructors
    File(string str = "") {
        int c = 10;
        for(int i = 0; i<c; i++) {
            name[i] = ' ';
        }
        strcpy(name,str.c_str());
        count = 0;
        hasTag = false;
    }
    //setters and getters
    void setName(string str) {
        int c = 10;
        for(int i = 0; i<c; i++) {
            name[i] = ' ';
        }
        strcpy(name,str.c_str());
        count = 0;
    }
    string getName() const {
        return string(name);
    }
    //methods
    void addTag(Tag tag) {
        tags[count] = tag;
        count++;
        hasTag = true;
    }
    void write() {
        ofstream out("connections.txt", ios::binary|ios::app);
        out.write((char*)this, sizeof *this);
        out.close();
    }

    //additional methods
    void print() {
        cout << getName() << ":" << endl;
        for(int i = 0; i < count; i++) {
            cout << " " << tags[i].getName() << endl;
        }
        cout << "_______\n" << endl;
    }

};

//Print files and tags

void findWithoutTags(list<string> files) {
    ifstream in("connections.txt", ios::binary);
    File tmpFile;
    vector<File> con;
    while(in.read((char*)&tmpFile, sizeof tmpFile)) {
        con.push_back(tmpFile);
    }
    in.close();
    for(auto il = files.begin(); il != files.end(); il++) {
        for(auto iv = con.begin(); iv != con.end(); iv++) {
            if((!il->compare(iv->getName())) && (!iv->hasTag)) {
                cout << *il << ":\n" << "_______\n" << endl;
            }
        }
    }
}

void printByTag(list<string> files, string tag) {
    ifstream in("connections.txt", ios::binary);
    File tmpFile;
    vector<File> con;
    while(in.read((char*)&tmpFile, sizeof tmpFile)) {
        con.push_back(tmpFile);
    }
    in.close();
    for(auto il = files.begin(); il != files.end(); il++) {
        for(auto iv = con.begin(); iv != con.end(); iv++) {
            if(!il->compare(iv->getName())) {
                for(int i = 0; i < iv->count; i++) {
                    if(!iv->tags[i].getName().compare(tag)) {
                        iv->print();
                    }
                }
            }
        }
    }

}


int main() {
    truncFile(true);
    truncFile(false);
    list<string> files = myGetFileNames("dir");
    //Print list:
    //copy(files.begin(), files.end(), ostream_iterator<string>(cout, "\n"));

    Tag tag1,tag2,tag3;
    tag1.setName("name1");
    tag2.setName("name2");
    tag3.setName("name3");
    tag1.write();
    tag2.write();
    tag3.write();

    File file0("dir0");
    File file1("dir1");
    File file2("dir2");
    File file3("dir3");
    File file4("dir4");


    file0.addTag(tag1);
    file0.addTag(tag3);
    file3.addTag(tag2);

    file0.write();
    file1.write();
    file2.write();
    file3.write();
    file4.write();

    /*cout << "Files:\n";
    file0.print();
    file1.print();
    file2.print();
    file3.print();
    file4.print();*/

    findWithoutTags(files);
    printByTag(files,"name1");
    return 0;
}

