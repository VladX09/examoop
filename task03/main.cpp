/**
* Резьбовое соединение заданного диаметра состоит из одного винта заданной длины, нескольких шайб и гаек,
* Выделите общие свойства и методы перечисленных деталей в виртуальный базовый класс и предоставьте интерфейс
* для создания/модификации соединений
*/

#include <iostream>
#include <list>

using namespace std;


enum TIP {
    SOEDINENIE_Screw,
    SOEDINENIE_Washer,
    SOEDINENIE_Nut
};

class Thread {
protected:
    const double DIAMETER;
public:
    Thread(double diameter) : DIAMETER(diameter) {
    }
    virtual double getDiameter() const {
        return DIAMETER;
    }
    virtual TIP getType() const = 0;
};

class Screw: public Thread {
protected:
    const double OUTER_DIAMETER;
public:

    Screw(double ScrewD, double ThreadD) : Thread(ThreadD), OUTER_DIAMETER(ScrewD) {
    }

    virtual TIP getType() const {
        return SOEDINENIE_Screw;
    }
};

class Washer: public Thread {
protected:
    const double OUTER_DIAMETER;
public:

    Washer(double ScrewD, double ThreadD) : Thread(ThreadD), OUTER_DIAMETER(ScrewD) {
    }

    virtual TIP getType() const {
        return SOEDINENIE_Washer;
    }
};

class Nut: public Thread {
protected:
    const double OUTER_DIAMETER;
public:

    Nut(double ScrewD, double ThreadD) : Thread(ThreadD), OUTER_DIAMETER(ScrewD) {
    }

    virtual TIP getType() const {
        return SOEDINENIE_Nut;
    }
};


class Connection {
private:
    const Screw& mScrew;
    list<Thread*> elements;
public:
    Connection(const Screw& Screw) : mScrew(Screw) {
    }

    void addNut(Nut& Nut) {
        elements.push_back((dynamic_cast<Thread*>(&Nut)));
    }
    void addWasher(Washer& Washer) {
        if (mScrew.getDiameter() == Washer.getDiameter())
            elements.push_back((dynamic_cast<Thread*>(&Washer)));
    }

    void removeLast() {
        if (!elements.empty()) {
            elements.pop_back();
        }
    }

};


int main() {
    Screw screw(10, 5);
    Nut nut1(12, 5), nut2(14, 5);
    Washer washer1(23, 5), washer2(14, 5);
    Connection a(screw);
    a.addNut(nut1);
    a.addNut(nut2);
    a.addWasher(washer1);
    a.removeLast();
    a.addWasher(washer2);
    return 0;
}