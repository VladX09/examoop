#include <iostream>
#include <string>
#include <fstream>
#include <vector>
using namespace std;

class Time {
private:
    int seconds;
public:

    Time(int sec){
        seconds = sec;
    }
    Time(){
        seconds = 0;
    }

    int getSeconds(){
        return seconds;
    }
    void setSeconds(int sec){
        seconds = sec;
    }
    int getHours(){
        return seconds/3600;
    }
    int getMinutes(){
        return seconds%3600/60;
    }
    string getFormatted(){
        int h = getHours();
        int m = getMinutes();
        string hours = (h>=10) ? to_string(h) : (string("0") + to_string(h));
        string minutes = (m>=10) ? to_string(m) : (string("0") + to_string(m));
        return hours + string(":") + minutes;
    }
};

class Task {
public:
    int startSec;
    int endSec;
    string name;

    Task(){
       startSec = 0;
       endSec = 0;
       name = "";
    }

   static vector<Task> parseFile(){
        vector<Task> Tasks;
        ifstream in;
        in.open("data.txt");        
        Task task;
        string startTime, endTime;
        while(!in.eof()){
            in >> startTime;
            in >> endTime;
            in >> task.name;
            cout << "Parsing\n<<<<" << endl;
            task.startSec = parseSec(startTime);
            task.endSec = parseSec(endTime);
            cout << ">>>>>" << endl;
            Tasks.push_back(task);
        }
        return Tasks;

    }

    static int parseSec(string Time){                         //Add exception for wrong input?
        string HH = Time.substr(0,2);
        string MM = Time.substr(3,2);
        return atoi(HH.c_str()) * 3600 + atoi(MM.c_str()) * 60;
    }

    bool checkTask(Time t1, Time t2){
        bool res = true;
        if(this->startSec > t2.getSeconds()){
            res = false;
        }
        if(this->endSec < t1.getSeconds()){
            res = false;
        }
        return res;
    }
};

int main()
{
    vector<Task> Tasks;
    Tasks = Task::parseFile();
    for( int t = 7*3600; t < 24*3600; t+=15*60){

        Time t1(t);        
        Time t2(t+15*60);

        cout << t1.getFormatted() << "-" << t2.getFormatted();

        for (vector<Task>::iterator it = Tasks.begin(); it < Tasks.end(); it++){

            if(it->checkTask(t1,t2)){
                cout << "*";
                break;
            }

        }
        cout << endl;

    }
    return 0;
}
